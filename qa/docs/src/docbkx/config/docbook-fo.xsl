<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version='1.0'>
	<xsl:import href="urn:docbkx:stylesheet" />
	<xsl:param name="body.font.family">Droid Sans Fallback</xsl:param>
	<xsl:param name="monospace.font.family">Droid Sans Fallback</xsl:param>
	<xsl:param name="title.font.family">Droid Sans Fallback</xsl:param>
</xsl:stylesheet>