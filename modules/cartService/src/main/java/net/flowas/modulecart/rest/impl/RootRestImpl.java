package net.flowas.modulecart.rest.impl;

import net.flowas.modulecart.rest.CategoryEndpoint;
import net.flowas.modulecart.rest.ConfigerationEndpoint;
import net.flowas.modulecart.rest.ProductEndpoint;
import net.flowas.modulecart.rest.RootRest;

public class RootRestImpl implements RootRest {

	CategoryEndpoint categoryEndpoint=new CategoryEndpoint();
	ProductEndpoint productEndpoint=new ProductEndpoint();
	ConfigerationEndpoint configerationEndpoint=new ConfigerationEndpoint();
	@Override
	public CategoryEndpoint getCategoryEndpoint() {
		return categoryEndpoint;
	}

	@Override
	public ProductEndpoint getProductEndpoint() {
		return productEndpoint;
	}

	@Override
	public ConfigerationEndpoint getConfigerationEndpoint() {
		return configerationEndpoint;
	}
}
