'use strict';

/*
*				Main Controller
*/
shopApp.controller('MainCtrl', function($scope,mainProductsList, mainPromoList,mainCategory, $http, $templateCache,$location, $timeout) {
	//.. Products
	// get products
	$scope.list = mainProductsList.query();
	// get promos
	$scope.promos = mainPromoList.query();
	// get categories
	$scope.categories = mainCategory.query();
	// Product Detail
	$scope.Message = "Product Detail";
	$scope.Product = {
		name:"", description:"", price: 0, image:"0.jpg", item: null
	};
	$scope.setProduct = function(item){
		$scope.Product.name = item.name;
		$scope.Product.description =  item.description;
		$scope.Product.price = item.price;
		$scope.Product.image = item.imagePath;
		$scope.Product.item = item;
	};


	//.. User Validation
	$scope.showLoginMessage =  false;
	$scope.errorLoginDetail = "verify user and password and try again";
	$scope.boxUser = {
		user:"",pass:""
	};	
	// clean error message
	$scope.cleanErrorMessage  = function(){
		$scope.showLoginMessage =  false;
	}
//**end
});

/*
*				Cart Controller
*/
shopApp.controller('CartCtrl',function($scope){
	// set filter
	$scope.cartFilter = {completed : true};

});
